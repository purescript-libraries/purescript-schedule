# purescript-schedule
Purescript bindings for [node-schedule](https://github.com/node-schedule/node-schedule).

The API usage is documented in test/Main.purs .

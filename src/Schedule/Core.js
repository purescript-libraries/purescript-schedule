 
'use strict';

var schedule = require('node-schedule');

function _scheduleLog(jobName, cronString, msg){
    return function(){
        schedule.scheduleJob(jobName, cronString, function(){
            console.log(msg);
        });
    };
};

function _scheduleJob(jobName, cronString, jobFunction){
    return function(){
        schedule.scheduleJob(jobName, cronString, jobFunction)
    }
}

function _rescheduleJob(jobName, cronString){
    return function(){
        schedule.rescheduleJob(jobName, cronString);
    }

}

function _jobName(job){
    return job.name;
};

function _jobsList(){
    return Object.keys(schedule.scheduledJobs);
}

function _runJob(jobName){
    return function(){
        var j = schedule.scheduledJobs[jobName];
        j.job();
    }
}

function _cancelJob(jobName){
    return function(){
        schedule.cancelJob(jobName);
    }
}


/* Public API */
exports._scheduleLog    = _scheduleLog;
exports._scheduleJob    = _scheduleJob
exports._jobsList       = _jobsList;
exports._rescheduleJob  = _rescheduleJob;
exports._cancelJob      = _cancelJob;
exports._runJob         = _runJob;
// exports._jobName        = _jobName; # not tested
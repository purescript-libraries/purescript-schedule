module Schedule.Core where

import Prelude

import Effect 
import Foreign
import Data.Function.Uncurried

foreign import data Job :: Type

foreign import _scheduleLog :: Fn3 String String String (Effect Job)
scheduleLog :: String -> String -> String -> Effect Job
scheduleLog = runFn3 _scheduleLog

foreign import _scheduleJob :: forall a. Fn3 String String (Effect a) (Effect Job)
scheduleJob :: forall a. String -> String -> Effect a -> Effect Job
scheduleJob = runFn3 _scheduleJob


foreign import _rescheduleJob :: Fn2 String String (Effect Job)
rescheduleJob :: String -> String -> Effect Job
rescheduleJob = runFn2 _rescheduleJob

-- foreign import _jobName :: Job -> String
foreign import _jobsList :: Effect (Array String)
foreign import _cancelJob :: String -> Effect (Boolean)
foreign import _runJob :: String -> Effect Unit
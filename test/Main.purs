module Test.Main where

import Prelude

import Foreign
import Effect (Effect)
import Effect.Console (log)
import Data.Foldable
import Test.Assert

import Schedule.Core

main :: Effect Unit
main = do
    log "scheduleLog schedules a message..."
    _ <- scheduleLog "Log1" "42 * * * * *" "Logged test message 1"
    _ <- scheduleLog "Log2" "30 * * * * *" "Logged test message 2"
    

    log "more generally, scheduleJob allows scheduling any function of type Effect a"
    _ <- scheduleJob "Log3" "11 * * * * *" (log "Logged test message 3")


    log "_jobsList contains the names of the scheduled jobs..."
    jobs <- _jobsList
    assertTrue $ elem "Log1" jobs
    assertTrue $ elem "Log2" jobs
    assertTrue $ elem "Log3" jobs
    log $ show jobs


    log "_rescheduleJob allows updating the cronString for a job by name"
    _ <- rescheduleJob "Log3" "43 * * * * *" --should now appear after Log1
    -- stillTwoJobs <- _jobsList
    -- log $ show stillTwoJobs


    log "_cancelJob allows cancelling a job by name"
    success <- _cancelJob "Log2"
    twoJobs <- _jobsList
    assertFalse $ elem "Log2" twoJobs
    log $ show twoJobs


    log "_runJob allows running a job by name on invocation"
    _ <- _runJob "Log3"


    -- Cleaning so node-schedule allows the script to finish.
    -- Comment out the following lines to watch the logged messages appear.
    _ <- _cancelJob "Log1"
    _ <- _cancelJob "Log3"


    pure unit

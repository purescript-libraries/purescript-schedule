with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [
    git
    gitAndTools.gitflow

    nodejs

    python36 # For running automatic tests
    python36Packages.watchdog
  ];

  shellHook = ''
    export NPM_CONFIG_PREFIX="$HOME/.global_node_modules"
    export PATH="$NPM_CONFIG_PREFIX/bin:$PATH"
  '';
}
